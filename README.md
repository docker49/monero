# monero

This project is a simple compose file which deploy a monero node.

## Deployment

* Create a configuration directory and copy the monerod.conf in it
  * you can replace the field **rpc-login**
* Create a data directory
* Fill the .env with required information
* Start the container:

  ```bash
  docker-compose up -d
  ```

## Remark

* the synchronization can take some days